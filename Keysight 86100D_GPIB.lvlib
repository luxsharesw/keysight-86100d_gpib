﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)F!!!*Q(C=\&gt;4"51*"%)8BJ_8"+RF9J0"3)!63["1Y5?72&amp;$I&amp;5C#&amp;4I%53'(^&gt;WD25AM];*5(:RVEX]T/@$M\L$4+I`3A[6&lt;:8CVT__AT?E\&lt;S^FL-GV(0FU[^^HLR`3O]`&lt;,^L?4L^M`T`DR\+L`6N(6]N`_X`[XWW^P\_@&lt;0Y,@,&gt;,&gt;E&amp;+,GN3A?NT&gt;6/2&amp;8O2&amp;8O2&amp;HO2*HO2*HO2*(O2"(O2"(O2"&lt;H+4G^TE*D@Z/-B&amp;,H+2MSL&amp;Z-6%R;$&amp;!%6H+#I_#E`B+4S&amp;BV-6HM*4?!J0Y;',#E`B+4S&amp;J`"QG1J0Y3E]B;@Q-.31V$D)]21?BF@C34S**`%E(K:5YEE!S74*Q-EA-*1U*F]34_**0(R6YEE]C3@R*"[;F8A34_**0)G(3];KZ.$-"TE?BF(A#4S"*`!%(I:7Y!E]A3@Q""[G5_!*0!%CG$!9()+#CY)/Q5HA#4T]5_!*0)%H]!1?GM94CL%SMW9_S0%9D`%9D`%9$U0)?)T(?)T(?"B7RG-]RG-]RM.5-B\D-2Y$-:-SP=RAZE,4S14'Q^`9,2Z0+9@%Y_C@ZHGDKD?A?G/J.YR[)[A@M0L"K2_)?K(6#[B?'05.KW^%$620L"Z1X6%H0I`5!X60X6%XV$6V26V3&amp;`/F0^TR&gt;$LJ?$TK=$BIP^^LN^NJM^FIP6ZLN6JJO6RKM6B=XF:0(/&gt;S^^XXUD.^@[/?XUN`Y&gt;WI?UXP8P/MU1P0J,;R!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="Keysight 86100D_GPIB.lvclass" Type="LVClass" URL="../Keysight 86100D_GPIB.lvclass"/>
</Library>
