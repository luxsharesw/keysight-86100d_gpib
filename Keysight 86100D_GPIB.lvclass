﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">Keysight 86100D_GPIB.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../Keysight 86100D_GPIB.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)F!!!*Q(C=\&gt;4"51*"%)8BJ_8"+RF9J0"3)!63["1Y5?72&amp;$I&amp;5C#&amp;4I%53'(^&gt;WD25AM];*5(:RVEX]T/@$M\L$4+I`3A[6&lt;:8CVT__AT?E\&lt;S^FL-GV(0FU[^^HLR`3O]`&lt;,^L?4L^M`T`DR\+L`6N(6]N`_X`[XWW^P\_@&lt;0Y,@,&gt;,&gt;E&amp;+,GN3A?NT&gt;6/2&amp;8O2&amp;8O2&amp;HO2*HO2*HO2*(O2"(O2"(O2"&lt;H+4G^TE*D@Z/-B&amp;,H+2MSL&amp;Z-6%R;$&amp;!%6H+#I_#E`B+4S&amp;BV-6HM*4?!J0Y;',#E`B+4S&amp;J`"QG1J0Y3E]B;@Q-.31V$D)]21?BF@C34S**`%E(K:5YEE!S74*Q-EA-*1U*F]34_**0(R6YEE]C3@R*"[;F8A34_**0)G(3];KZ.$-"TE?BF(A#4S"*`!%(I:7Y!E]A3@Q""[G5_!*0!%CG$!9()+#CY)/Q5HA#4T]5_!*0)%H]!1?GM94CL%SMW9_S0%9D`%9D`%9$U0)?)T(?)T(?"B7RG-]RG-]RM.5-B\D-2Y$-:-SP=RAZE,4S14'Q^`9,2Z0+9@%Y_C@ZHGDKD?A?G/J.YR[)[A@M0L"K2_)?K(6#[B?'05.KW^%$620L"Z1X6%H0I`5!X60X6%XV$6V26V3&amp;`/F0^TR&gt;$LJ?$TK=$BIP^^LN^NJM^FIP6ZLN6JJO6RKM6B=XF:0(/&gt;S^^XXUD.^@[/?XUN`Y&gt;WI?UXP8P/MU1P0J,;R!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.6</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"P&gt;5F.31QU+!!.-6E.$4%*76Q!!&amp;W!!!!2^!!!!)!!!&amp;U!!!!!]!!!!!BJ,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!!!!I"=!A!!!-!!!+!!!!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!#M%&lt;1I+FO71*@_K,QU.!M$!!!!$!!!!"!!!!!!?S53*D/5;%K5Q&gt;^X$L9[&lt;N1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!-Y3RU^8&gt;]:*E/*9I\2]&amp;45"!!!!`````^1&gt;D.G0!,)%[9!*G/TY1HY!!!!1B2DT&lt;**^R;[#:C\&lt;/]W1PA!!!!1!!!!!!!!!W!!"4&amp;:$1Q!!!!)!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!F:*5%E!!!!!!R:*&lt;H.U=H6N:7ZU)%.P&lt;7UO&lt;(:M;7*Q$&amp;:*5U%O&lt;(:D&lt;'&amp;T=Q)(!!"16%AQ!!!!8!!"!!Y!!!!!!!!$28BF$%RV?(.I98*F,5^&amp;6!F-;7*S98*J:8-(5'RV:WFO=R:*&lt;H.U=H6N:7ZU)%.P&lt;7UO&lt;(:M;7*Q"5.M98.T"&amp;:*5U%-6EF413ZM&gt;G.M98.T!!!!!!!"!!!!!!!"!!%!!!!!!A!!!!!!!!!!!Q!!!!)!!1!!!!!!)Q!!!"RYH'.A:W"K9,D!!-3-1"98E-7E!?2^9""A!!"7,A9R!!!!!"1!!!!+?*RD9'&lt;A:/!!1A9!!+1!(1!!!%E!!!%9?*RD9-!%`Y%!3$%S-$"&gt;!^+M;/*A'M;G*M"F,C[\I/,-1-S#Z%[A'.-?)-U%%I?K99.)-6U!YB0IZP"$[1&gt;)9A#4(3E&lt;!!!!!!!!$!!"6EF%5Q!!!!!!!Q!!!==!!!0)?*T&lt;Q-D!E'FM9=&lt;!R-$!$'3,-T1Q*/?HJ0)S!0E-%+!$9V!!!K$G;;'*'RYYH!9%?PTS,7"_]RO?&lt;B=6A?9;&amp;1GG5J&amp;O(R724B]6FEY7F2&gt;``P``XXS%ZX#X2]ZR2RO1WGY/I0BR&amp;R5/%!&gt;)MY$I`Y%:)&amp;7IZMFU!G7"N!33"LC"+09(!&amp;6R.&amp;1I-Z3Q'"[)/HS]Q912YF#9%[+QO:&gt;Y]ZP@=!!^*8$Q)5NXIQ;1XTM22!+&amp;?$J$/#3/OX$IC!(ZD#&gt;!"H&lt;SQ(T.!@&gt;0'-C!%B7"4B/123S--)O[W9Y\;)$$Q5%%1G6!K!I)61#C&gt;I"&gt;=)1D\D!]`.?_PL?,&amp;5CT)=7*!R!XA"B-K&amp;C0A:'"%=RE:&amp;A,67M$:$."R7"R#W)L1)/.E=%?LO=W6&amp;Y$S2Q82JA?B,JK*(=QA=VA:0D$!$-0;"^54Q05X3!R8[$9!3A\"-C?!'6(!^E@I/QE)&amp;M!SMY%MAU9)?Q]+"NM'1.OWNH@R25JG-$Z!J9VL)!Y/&lt;@!Q%#PWFH(3=?JVLL;W=9TL\CEK$1X.;^%Q4E`.V=PJSQH-[H!+MQTW"()4MZ*,#[W!SNG9,!%[3^),C.4/Q-!W('R:Q!!!!%0!!!"&lt;(C==W"A9-AUND"\!+3:'2E9R"E;'*,T5V):E)!))Q./%"Q?VPR'I,N%2;'T2)7HOU:&amp;J&lt;.'B1/)74J.6&amp;\]_@``@_M"`CE(3FDZNRVY(&gt;0LRN,JIM,4[]`3[;0#U6$0QM!`^6"$(9A[X((0Y*L"Z2)/`GX(GD]QPD&lt;I$7$M$!'K$122(!W6D%"&amp;"RMK1.3"DE-'J`CX87L_R'BQ9A@)&gt;@SOBZK0#-4&amp;2R^'=^`;V`&gt;WA21A?]%"C$]$25"C4%!MBS1/!M\_,K\I`A?JN1,CZ.Q#!Q/^;G=&gt;*RWH7ONK:RP0P/+3IN,=V,Q3"?@]X&amp;S^H,+=T+1#KT$09%=A/TEHM&lt;D9$KS9A1%!'828/!!!!!$=!!!",(C==W"A9-AUND"&lt;!+3:'2E9R"E;'*,T5V):E-!6"NQA0+TZD5"XC9J-:YE+4X?.CE*HD1I(%,.UGKC]_00````7![6'P7YMH3YK0,XO))KDSYWFQZWF'S4AQ&gt;DJ!^4GI],2']D9'1+5=W$M='1U0."]2#!O`D$1`,7P\_U#/IO"%=F/"S"_#R1"C4%"M3S3/!AY_\OYILM@J.93C*-,EMPUKJVVH(3=;KWLH7U]]YJ,CEJT5`.+&amp;*TT=X0V=MJS-J-+L-)]ARW"\/3=R/*C/\"CI&amp;Y!9$V!*Q!!!!Y8!9!1!!!'-4=O-#YR!!!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Y8!9!1!!!'-4=O-#YR!!!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Y8!9!1!!!'-4=O-#YR!!!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A'!!!9'9!!''"A!"G!'!!;!!1!'Q!-!"L!.!!;-/Q!'A^5!"I#L!!;!V1!'A+M!"I$6!!;!KQ!'A.5!"G#O!!99W!!'"O!!"A'!!!@````]!!!1!````````````````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!#QM!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!#[V@C;U,!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!#[V@.45V.9GN#Q!!!!!!!!!!!!!!!!!!!!$``Q!!#[V@.45V.45V.47*L1M!!!!!!!!!!!!!!!!!!0``!)F@.45V.45V.45V.45VC;U!!!!!!!!!!!!!!!!!``]!8V]V.45V.45V.45V.48_C1!!!!!!!!!!!!!!!!$``Q"@C9F@.45V.45V.48_`PZ@!!!!!!!!!!!!!!!!!0``!&amp;_*C9G*8T5V.48_`P\_`F]!!!!!!!!!!!!!!!!!``]!8YG*C9G*C6_N`P\_`P\_8Q!!!!!!!!!!!!!!!!$``Q"@C9G*C9G*C@\_`P\_`PZ@!!!!!!!!!!!!!!!!!0``!&amp;_*C9G*C9G*`P\_`P\_`F]!!!!!!!!!!!!!!!!!``]!8YG*C9G*C9H_`P\_`P\_8Q!!!!!!!!!!!!!!!!$``Q"@C9G*C9G*C@\_`P\_`PZ@!!!!!!!!!!!!!!!!!0``!&amp;_*C9G*C9G*`P\_`P\_`F]!!!!!!!!!!!!!!!!!``]!C9G*C9G*C9H_`P\_`P[*C1!!!!!!!!!!!!!!!!$``Q!!8V_*C9G*C@\_`P[*L6]!!!!!!!!!!!!!!!!!!0``!!!!!&amp;_*C9G*`P[*C6]!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!"@C9G*C45!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!8T5!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!!)!!1!!!!!"AA!"2F")5!!!!!)!!E:15%E!!!!$&amp;EFO=X2S&gt;7VF&lt;H1A1W^N&lt;3ZM&gt;GRJ9H!-6EF413ZM&gt;G.M98.T!A=!!&amp;"53$!!!!"=!!%!$A!!!!!!!!.&amp;?'5-4(6Y=WBB=G5N4U65#5RJ9H*B=GFF=Q&gt;1&lt;(6H;7ZT&amp;EFO=X2S&gt;7VF&lt;H1A1W^N&lt;3ZM&gt;GRJ9H!&amp;1WRB=X-%6EF411R736.",GRW9WRB=X-!!!!!!!%!!!!!!!%!!1!!!!!#!!!!!!!!!!!!!!!!!1!!!$-!!E2%5%E!!!!!!!-737ZT&gt;(*V&lt;76O&gt;#"$&lt;WVN,GRW&lt;'FC=!R736.",GRW9WRB=X-#"Q!!5&amp;2)-!!!!&amp;Q!!1!/!!!!!!!!!U6Y:1R-&gt;8BT;'&amp;S:3V0261*4'FC=G&amp;S;76T"V"M&gt;7&gt;J&lt;H-737ZT&gt;(*V&lt;76O&gt;#"$&lt;WVN,GRW&lt;'FC=!6$&lt;'&amp;T=Q2736."$&amp;:*5U%O&lt;(:D&lt;'&amp;T=Q!!!!!!!1!!!!!!!1!"!!!!!!)!!!!!!!"J!!!!!!!"!!!!+A!$!!!!!!0Q!!!+\XC=X::.;"."&amp;-@@*'G:F":HW[Z;M':&lt;.L')F@KNR?^MV8[IL7EVCKD"6+V%+`U14R9B#$U)AF"2^/$"ACBY[%&amp;"4R+]\%&amp;0(P11W[OA(B1,OFH@T':X]V&amp;455`G-#TB`&gt;_&lt;_&lt;`@TFO!CA^MM3=$FQUA\#M_&gt;"F1%&gt;=*1+K:1P98OAKMDXQ(5FN($.B/_^A\4Y9M-;!SLI&gt;ICT9/HT(;@'`?*/5EQK9RN*T69&lt;)+!R&lt;%^563BZJG[O-F[HC:H67#!,N'-J[I'PB'LS5(M3!EG`AK.:--%+X"ZUMW&gt;M@/^S&gt;6`K_`G&gt;;*F(Y$G+:8$;HJ::A23\]1+=F$)J0H&gt;EL!F%UQ.48FCC2,&amp;",&lt;W)A;)K-K2"[7U&amp;2L?PWQGFYB."6#AX6#&gt;BUNE,H"^]Z&amp;B&gt;);47=I2&gt;WF\*'ZVN9:WY6O:G9'&gt;&lt;BG&gt;7=-K&amp;84/WE&gt;H9[`,;PK34Y"!C4Z'M!U5]?J_=S=Q#S0J(W]&amp;U*2D?U)NB)7Q??)!=O4OO=%_'S4(]!&gt;U1S@X9T.W!SS3T4$LYUT=,NR:ZZO;,Z59XUY-4I]UD_E$*Z34C:CQ]0+B;'"C\'2@C5?'YE6^WG,JPMX=!^Y-9%)S/#$_X!UV`."G*S=2"NQ&gt;;6&lt;5&lt;J)44M[3:TGB/.\X07&gt;6X8^WY&lt;_;&lt;/^J\G(Q6;04;Z8E,N'L"VCX?F3P"IJFFDB&amp;P-I8P@P+6[0=&amp;UOI"CC="U'3B#ZQ2+Z&amp;'-]Q$W)FN"M2-V9,M7IC;*G9([+.R62T,5&amp;&amp;%^-4/4JM!ON$M5_1GS+T2`G$U\R?`-D0)5B1&lt;&amp;8+0:D/XD_+/9@Q`RZT/\HT/Y7VMOZT$,XU(.\LZ3$R7X6XND)';68;QPX+'U^6C&gt;9U(K(-",5*I+&lt;$4L.K43A'`H$6V'WI;A&amp;$Q3B"E_\N"/L]&amp;5+7,ME';_3#NA]3%F&amp;B$4QF46G$5\12J[\/$2O]F"=ZQ`^_[T_A(P!(OP&gt;0#2-83"-L5&lt;^-J!&gt;%O$5H'?5-512)1U]R#H28\S&lt;P&amp;$T^U.P`8GI=U;E]!"3'$Q-Q3/AT2[IN7Y%,^Y)6Y^RH\*RS&amp;Y(0XW,?0P7ZL-8_16\%A;7:M_4:9^'"M[.*P#/,,A!M!7^"9T*S&amp;DI@W+MLZCR'N1X`5_-(:S$-6]"9Q3^7+0J#NA@3J_Q1AO%]VHL`/6M$P`G06&gt;ZM$WS9W8CIJD.R1/H3^0J+D5&gt;Q`T:%6;0IWY,K#6GRV[LB8Z\"O_!&amp;PPW`\*HTKG"HOT+];4KCO6*'8K#TOX,K58@P2RV0D&amp;J2.8-5A'M$:/0'3"LY?J8L"W&gt;K[4NN)X&gt;28^RH#_E&gt;_H8V"PH)T46Z3DR_WH7?V;^(;#,@Q)$7&gt;9!!!!!"!!!!$5!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!$JA!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!!KB=!A!!!!!!"!!A!-0````]!!1!!!!!!DA!!!!1!0%"Q!"Y!!#5737ZT&gt;(*V&lt;76O&gt;#"$&lt;WVN,GRW&lt;'FC=!R736.",GRW9WRB=X-!$&amp;:*5U%O&lt;(:D&lt;'&amp;T=Q!!$E!B#&amp;.J&lt;86M982F!!!31#%.47&amp;U;#"52%6$53"&amp;51!K1&amp;!!!Q!!!!%!!BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!!!"!!-!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!-2=!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!Q!!!!!!!!!"!!!!!A!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!X.IY4!!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$=WDB-!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!+I8!)!!!!!!!1!)!$$`````!!%!!!!!!)Y!!!!%!$R!=!!?!!!F&amp;EFO=X2S&gt;7VF&lt;H1A1W^N&lt;3ZM&gt;GRJ9H!-6EF413ZM&gt;G.M98.T!!R736.",GRW9WRB=X-!!!Z!)1B4;7VV&lt;'&amp;U:1!!%E!B$5VB&gt;'AA6%2&amp;1V%A26%!+E"1!!-!!!!"!!)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!!!1!$!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G5!!!!:&amp;Q#!!!!!!!%!"1!$!!!"!!!!!!!'!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U91!!!-Q8!)!!!!!!"!!]1(!!(A!!*2:*&lt;H.U=H6N:7ZU)%.P&lt;7UO&lt;(:M;7*Q$&amp;:*5U%O&lt;(:D&lt;'&amp;T=Q!-6EF413ZM&gt;G.M98.T!!!/1#%)5WFN&gt;7RB&gt;'5!!"*!)1V.982I)&amp;2%25.2)%62!#J!5!!$!!!!!1!#(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!!!%!!Q!!!!%F&amp;EFO=X2S&gt;7VF&lt;H1A1W^N&lt;3ZM&gt;GRJ9H!-6EF413ZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"1!-!!!!"!!!!&amp;9!!!!I!!!!!A!!"!!!!!!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!5A!!!)]?*S65-N/QE!5082Y3R&amp;1%9W;[9+.#Y)R-3R=6!&amp;.IS9ARKV7WE+4+36U3H4HTH]U@I4_A:?#-5:&gt;E*0=H0O9?]Z=!&amp;MYVM@9![JF9R4)3?D:)]G&lt;PO@6R&amp;3Y$_0=L&gt;%\)&gt;Y8:B$A:Y;]LK6\LB=+5^J!3&gt;@5+V-/_5WLX?TS&gt;B=P_(B\&lt;6Q#9.M8^F0A$I;3.YY/[P87X8H(/*VL\0T4CE1K@T&lt;\5K#G&gt;]"I&gt;1T+&lt;F/%A&lt;1HX(&gt;Y^)K0*_[54((,F#93"'2BU?Q\YJ3Q29J$**(25W#/'##N0T0`7MY,NO_A%#GEE5'7B:;$'J9Y&amp;1FJCQUZK&amp;#:@,2)N1-&amp;+]A4[Z/_1O\XF`C(!AL*79C"D.VD*H.'3TF+7#-_QTJ.@#'_Q/`+&gt;W=DQPQA-:3J"B4)5A)JL")L9B/6K&amp;=B8IR-YR.`,7W!!!!!HA!"!!)!!Q!&amp;!!!!7!!0!!!!!!!0!/U!YQ!!!'Y!$Q!!!!!!$Q$N!/-!!!#%!!]!!!!!!!]!\1$D!!!!GI!!A!#!!!!0!/U!YQ!!!*Q!$I!!A!!!$A$8!.%647FD=G^T&lt;W:U)%JI:7ZH3'6J)&amp;6*&amp;5VJ9X*P=W^G&gt;#"+;'6O:UBF;3"6326.;7.S&lt;X.P:H1A3GBF&lt;G&gt;):7EA65E"-!%Q!!"35V*$$1I!!UR71U.-1F:8!!!89!!!"(U!!!!A!!!81!!!!!!!!!!!!!!!)!!!!$1!!!2E!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%.$5V1!!!!!!!!"H%R*&gt;GE!!!!!!!!"M%.04F!!!!!!!!!"R&amp;2./$!!!!!"!!!"W%2'2&amp;-!!!!!!!!#!%R*:(-!!!!!!!!#&amp;&amp;:*1U1!!!!#!!!#+(:F=H-!!!!%!!!#:&amp;.$5V)!!!!!!!!#S%&gt;$5&amp;)!!!!!!!!#X%F$4UY!!!!!!!!#]'FD&lt;$A!!!!!!!!$"%.11T)!!!!!!!!$'%R*:H!!!!!!!!!$,%:13')!!!!!!!!$1%:15U5!!!!!!!!$6&amp;:12&amp;!!!!!!!!!$;%R*9G1!!!!!!!!$@%*%3')!!!!!!!!$E%*%5U5!!!!!!!!$J&amp;:*6&amp;-!!!!!!!!$O%253&amp;!!!!!!!!!$T%V6351!!!!!!!!$Y%B*5V1!!!!!!!!$^&amp;:$6&amp;!!!!!!!!!%#%:515)!!!!!!!!%(!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!`````Q!!!!!!!!$E!!!!!!!!!!$`````!!!!!!!!!0A!!!!!!!!!!0````]!!!!!!!!"!!!!!!!!!!!!`````Q!!!!!!!!(=!!!!!!!!!!$`````!!!!!!!!!?1!!!!!!!!!!P````]!!!!!!!!#$!!!!!!!!!!!`````Q!!!!!!!!)E!!!!!!!!!!$`````!!!!!!!!!H1!!!!!!!!!!0````]!!!!!!!!#B!!!!!!!!!!"`````Q!!!!!!!!21!!!!!!!!!!,`````!!!!!!!!"71!!!!!!!!!"0````]!!!!!!!!'2!!!!!!!!!!(`````Q!!!!!!!!:9!!!!!!!!!!D`````!!!!!!!!"GA!!!!!!!!!#@````]!!!!!!!!'@!!!!!!!!!!+`````Q!!!!!!!!;-!!!!!!!!!!$`````!!!!!!!!"K!!!!!!!!!!!0````]!!!!!!!!'O!!!!!!!!!!!`````Q!!!!!!!!&lt;-!!!!!!!!!!$`````!!!!!!!!"V!!!!!!!!!!!0````]!!!!!!!!,6!!!!!!!!!!!`````Q!!!!!!!!N=!!!!!!!!!!$`````!!!!!!!!$/1!!!!!!!!!!0````]!!!!!!!!1W!!!!!!!!!!!`````Q!!!!!!!"$A!!!!!!!!!!$`````!!!!!!!!%/A!!!!!!!!!!0````]!!!!!!!!1_!!!!!!!!!!!`````Q!!!!!!!"&amp;A!!!!!!!!!!$`````!!!!!!!!%7A!!!!!!!!!!0````]!!!!!!!!6&amp;!!!!!!!!!!!`````Q!!!!!!!"5=!!!!!!!!!!$`````!!!!!!!!&amp;31!!!!!!!!!!0````]!!!!!!!!65!!!!!!!!!#!`````Q!!!!!!!";=!!!!!"B,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!BJ,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!!!!$A!"!!!!!!!!!1!!!!%!(E"1!!!83W6Z=WFH;(1A/$9R-$"%,GRW9WRB=X-!!1!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!@``!!!!!1!!!!!!!1%!!!!"!"Z!5!!!&amp;UNF?8.J:WBU)$AW-4!Q2#ZM&gt;G.M98.T!!%!!!!!!!(````_!!!!!!)-5W.P='5O&lt;(:M;7*Q$6.D&lt;X"F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!"``]!!!!"!!!!!!!#!1!!!!)!%U!$!!R%;8.Q&lt;'&amp;Z)%FU:7U!!(1!]&gt;:\98!!!!!$&amp;5NF?8.J:WBU)$AW-4!Q2#ZM&gt;GRJ9B&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%1O&lt;(:D&lt;'&amp;T=R.,:8FT;7&gt;I&gt;#!Y.D%Q-%1O9X2M!#J!5!!"!!!&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!"!!!!!@````]!!!!!!!!!!AR49W^Q:3ZM&gt;GRJ9H!.5W.P='5O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!(``Q!!!!%!!!!!!!-"!!!!!1"S!0(7?Y6T!!!!!R6,:8FT;7&gt;I&gt;#!Y.D%Q-%1O&lt;(:M;7)83W6Z=WFH;(1A/$9R-$"%,GRW9WRB=X-43W6Z=WFH;(1A/$9R-$"%,G.U&lt;!!I1&amp;!!!"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!!!!!!!!!!!!!)-5W.P='5O&lt;(:M;7*Q$6.D&lt;X"F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!"``]!!!!"!!!!!!!!!A!!!!%!=A$RVHO&amp;=Q!!!!-63W6Z=WFH;(1A/$9R-$"%,GRW&lt;'FC&amp;UNF?8.J:WBU)$AW-4!Q2#ZM&gt;G.M98.T%UNF?8.J:WBU)$AW-4!Q2#ZD&gt;'Q!+%"1!!!&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!!!!!!!@````Y!!!!!!AR49W^Q:3ZM&gt;GRJ9H!.5W.P='5O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!(``Q!!!!%!!!!!!!%#!!!!!1"S!0(7?Y6T!!!!!R6,:8FT;7&gt;I&gt;#!Y.D%Q-%1O&lt;(:M;7)83W6Z=WFH;(1A/$9R-$"%,GRW9WRB=X-43W6Z=WFH;(1A/$9R-$"%,G.U&lt;!!I1&amp;!!!"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!!!!!!"`````A!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!@``!!!!!1!!!!!!!A)!!!!"!()!]&gt;:\B8-!!!!$&amp;5NF?8.J:WBU)$AW-4!Q2#ZM&gt;GRJ9B&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%1O&lt;(:D&lt;'&amp;T=R.,:8FT;7&gt;I&gt;#!Y.D%Q-%1O9X2M!#B!5!!!(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!!!!!!(````_!!!!!!)-5W.P='5O&lt;(:M;7*Q$6.D&lt;X"F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!"``]!!!!"!!!!!!!!!!!!!!%!=A$RVHO&amp;=Q!!!!-63W6Z=WFH;(1A/$9R-$"%,GRW&lt;'FC&amp;UNF?8.J:WBU)$AW-4!Q2#ZM&gt;G.M98.T%UNF?8.J:WBU)$AW-4!Q2#ZD&gt;'Q!+%"1!!!&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!!!!!!!@````Y!!!!!!AR49W^Q:3ZM&gt;GRJ9H!.5W.P='5O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!(``Q!!!!%!!!!!!!%!!!!!!1"S!0(7?Y6T!!!!!R6,:8FT;7&gt;I&gt;#!Y.D%Q-%1O&lt;(:M;7)83W6Z=WFH;(1A/$9R-$"%,GRW9WRB=X-43W6Z=WFH;(1A/$9R-$"%,G.U&lt;!!I1&amp;!!!"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!!!!!!"`````A!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!@``!!!!!1!!!!!!!A!!!!!#!$R!=!!?!!%E&amp;5FO=X2S&gt;7VF&lt;H1A1W^N&lt;3ZM&gt;GRJ9AR736.",GRW9WRB=X-!!!R736.",GRW9WRB=X-!!)-!]&gt;=[I@M!!!!$'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-93W6Z=WFH;(1A/$9R-$"%8U&gt;135)O9X2M!#J!5!!"!!!&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!"!!!!!@````]!!!!"*"6*&lt;H.U=H6N:7ZU)%.P&lt;7UO&lt;(:M;7)-6EF413ZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!(``Q!!!!%!!!!!!!-!!!!!!A!]1(!!(A!!*"6*&lt;H.U=H6N:7ZU)%.P&lt;7UO&lt;(:M;7)-6EF413ZM&gt;G.M98.T!!!-6EF413ZM&gt;G.M98.T!!#$!0(8/K(\!!!!!RJ,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,G.U&lt;!!K1&amp;!!!1!!(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!1!!!!(````_!!!!!31637ZT&gt;(*V&lt;76O&gt;#"$&lt;WVN,GRW&lt;'FC$&amp;:*5U%O&lt;(:D&lt;'&amp;T=Q!!!!!!!!!!!!!!!!!!!!)-5W.P='5O&lt;(:M;7*Q$6.D&lt;X"F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!!!"!!!!!!!%!!!!!!)!0%"Q!"Y!!#5737ZT&gt;(*V&lt;76O&gt;#"$&lt;WVN,GRW&lt;'FC=!R736.",GRW9WRB=X-!$&amp;:*5U%O&lt;(:D&lt;'&amp;T=Q!!AQ$RVTK^)!!!!!-;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=RB,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZD&gt;'Q!+E"1!!%!!"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!%!!!!"`````Q!!!!%F&amp;EFO=X2S&gt;7VF&lt;H1A1W^N&lt;3ZM&gt;GRJ9H!-6EF413ZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!!#$&amp;.D&lt;X"F,GRW&lt;'FC=!V49W^Q:3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!!!!!1!!!!!!"1!!!!!$!$R!=!!?!!!F&amp;EFO=X2S&gt;7VF&lt;H1A1W^N&lt;3ZM&gt;GRJ9H!-6EF413ZM&gt;G.M98.T!!R736.",GRW9WRB=X-!!!Z!)1B4;7VV&lt;'&amp;U:1!!B1$RWJ3@M!!!!!-;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=RB,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZD&gt;'Q!,%"1!!)!!!!"(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!A!!!!)!!!!!`````Q!!!!%F&amp;EFO=X2S&gt;7VF&lt;H1A1W^N&lt;3ZM&gt;GRJ9H!-6EF413ZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!!!!!!!!AR49W^Q:3ZM&gt;GRJ9H!.5W.P='5O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!9!!!!!"!!]1(!!(A!!*2:*&lt;H.U=H6N:7ZU)%.P&lt;7UO&lt;(:M;7*Q$&amp;:*5U%O&lt;(:D&lt;'&amp;T=Q!-6EF413ZM&gt;G.M98.T!!!/1#%)5WFN&gt;7RB&gt;'5!!"*!)1V.982I)&amp;2%25.2)%62!)=!]&gt;T;/%Q!!!!$'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-93W6Z=WFH;(1A/$9R-$"%8U&gt;135)O9X2M!#Z!5!!$!!!!!1!#(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!Q!!!!-!!!!!!!!!!@````]!!!!"*2:*&lt;H.U=H6N:7ZU)%.P&lt;7UO&lt;(:M;7*Q$&amp;:*5U%O&lt;(:D&lt;'&amp;T=Q!!!!!!!!!!!!!!!!!!!!!!!!)-5W.P='5O&lt;(:M;7*Q$6.D&lt;X"F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!#!!!!,5NF?8.J:WBU)$AW-4!Q2#ZM&gt;GRJ9DJ,:8FT;7&gt;I&gt;#!Y.D%Q-%1O&lt;(:D&lt;'&amp;T=Q!!!$*,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9DJ,:8FT;7&gt;I&gt;#!Y.D%Q-%1O&lt;(:D&lt;'&amp;T=Q</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.3</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"_!!!!!AR49W^Q:3ZM&gt;GRJ9H!.5W.P='5O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!6A!"!!U!!!!!!!.&amp;?'5-4(6Y=WBB=G5N4U65#5RJ9H*B=GFF=QJ*&lt;H.U=H6N:7ZU"6.D&lt;X"F$&amp;.D&lt;X"F,GRW&lt;'FC=!649W^Q:1V49W^Q:3ZM&gt;G.M98.T!!!!!!</Property>
	<Item Name="Keysight 86100D_GPIB.ctl" Type="Class Private Data" URL="Keysight 86100D_GPIB.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Accessor" Type="Folder">
		<Item Name="Math TDECQ EQ" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Math TDECQ EQ</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Math TDECQ EQ</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Math TDECQ EQ.vi" Type="VI" URL="../Accessor/Math TDECQ EQ Property/Read Math TDECQ EQ.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'3!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!)1V.982I)&amp;2%25.2)%62!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Math TDECQ EQ.vi" Type="VI" URL="../Accessor/Math TDECQ EQ Property/Write Math TDECQ EQ.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'3!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!31#%.47&amp;U;#"52%6$53"&amp;51";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
		<Item Name="Simulate" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Simulate</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Simulate</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Simulate.vi" Type="VI" URL="../Accessor/Simulate Property/Read Simulate.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1B4;7VV&lt;'&amp;U:1!!8%"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!93W6Z=WFH;(1A/$9R-$"%8U&gt;135)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Simulate.vi" Type="VI" URL="../Accessor/Simulate Property/Write Simulate.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!/1#%)5WFN&gt;7RB&gt;'5!!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
		<Item Name="VISA.lvclass" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">VISA.lvclass</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">VISA.lvclass</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read VISA.lvclass.vi" Type="VI" URL="../Accessor/VISA.lvclass Property/Read VISA.lvclass.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!']!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!F&amp;EFO=X2S&gt;7VF&lt;H1A1W^N&lt;3ZM&gt;GRJ9H!-6EF413ZM&gt;G.M98.T!!R736.",GRW9WRB=X-!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">268967936</Property>
			</Item>
			<Item Name="Write VISA.lvclass.vi" Type="VI" URL="../Accessor/VISA.lvclass Property/Write VISA.lvclass.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!']!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*2:*&lt;H.U=H6N:7ZU)%.P&lt;7UO&lt;(:M;7*Q$&amp;:*5U%O&lt;(:D&lt;'&amp;T=Q!-6EF413ZM&gt;G.M98.T!!";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8912896</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Control" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Measure Threhold Method Type-Enum.ctl" Type="VI" URL="../Control/Measure Threhold Method Type-Enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#,!!!!!1#$!0%!!!!!!!!!!R6,:8FT;7&gt;I&gt;#!Y.D%Q-%1O&lt;(:M;7)83W6Z=WFH;(1A/$9R-$"%,GRW9WRB=X-F476B=X6S:3"5;(*F;'^M:#".:82I&lt;W1A6(FQ:3V&amp;&lt;H6N,G.U&lt;!!H1"9!!Q&gt;1-4!V-$EQ"V!S-$5Q/$!%652&amp;2A!!"&amp;2Z='5!!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="Mode-Enum.ctl" Type="VI" URL="../Control/Mode-Enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#(!!!!!1"`!0%!!!!!!!!!!R6,:8FT;7&gt;I&gt;#!Y.D%Q-%1O&lt;(:M;7)83W6Z=WFH;(1A/$9R-$"%,GRW9WRB=X-.47^E:3V&amp;&lt;H6N,G.U&lt;!!\1"9!"!B&amp;?75P47&amp;T;QN0=W.J&lt;'^T9W^Q:1&gt;52&amp;)P6%25$%JJ&gt;(2F=C^/&lt;WFT:1!%47^E:1!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="Persistence-Enum.ctl" Type="VI" URL="../Control/Persistence-Enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#C!!!!!1#;!0%!!!!!!!!!!RJ,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T&amp;&amp;"F=H.J=X2F&lt;G.F,56O&gt;7UO9X2M!%6!&amp;A!##U.P&lt;'^S)%&gt;S972F#E&gt;S98EA5W.B&lt;'5!!#&amp;1:8*T;8.U:7ZD:3!I5W&amp;N='RF)%&amp;D9X6N&gt;7RB&gt;'FP&lt;CE!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="Slot Channel-Enum.ctl" Type="VI" URL="../Control/Slot Channel-Enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"I!!!!!1"A!0(7/M;$!!!!!R6,:8FT;7&gt;I&gt;#!Y.D%Q-%1O&lt;(:M;7)83W6Z=WFH;(1A/$9R-$"%,GRW9WRB=X-61WBB&lt;GZF&lt;#V$&lt;WVC&lt;S"#&lt;XAO9X2M!"2!-0````](1WBB&lt;GZF&lt;!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="Window State-Enum.ctl" Type="VI" URL="../Control/Window State-Enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#`!!!!!1#X!0%!!!!!!!!!!RJ,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T&amp;6&gt;J&lt;G2P&gt;S"4&gt;'&amp;U:3V&amp;&lt;H6N,G.U&lt;!"B1"9!"!J'&gt;7RM)&amp;&gt;J:(2I#EBB&lt;'9A6WFE&gt;'A947^S:3"*&lt;H2P)&amp;"S;7VB=HEA6WFO:'^X'%VP&gt;G5A&gt;']A5W6D&lt;WZE98*Z)&amp;&gt;J&lt;G2P&gt;Q!-6WFO:'^X)&amp;.U982F!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
	</Item>
	<Item Name="Override" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Measurement" Type="Folder">
			<Item Name="NRZ Measurement" Type="Folder">
				<Item Name="Get Average Power.vi" Type="VI" URL="../Override/Get Average Power.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'M!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(E!Q`````R24&gt;(*J&lt;G=[18:F=G&amp;H:3"1&lt;X&gt;F=A!!'U!+!"6/&gt;7VF=GFD/E&amp;W:8*B:W5A5'^X:8)!8%"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!93W6Z=WFH;(1A/$9R-$"%8U&gt;135)A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!=!#1-!!(A!!!U)!!!*!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!I!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
				<Item Name="Get Crossing.vi" Type="VI" URL="../Override/Get Crossing.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'A!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'%!Q`````Q^4&gt;(*J&lt;G=[1X*P=X.J&lt;G=!&amp;5!+!!^/&gt;7VC:8)[1X*P=X.J&lt;G=!8%"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!93W6Z=WFH;(1A/$9R-$"%8U&gt;135)A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!=!#1-!!(A!!!U)!!!*!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!I!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
				<Item Name="Get Extinction Ratio.vi" Type="VI" URL="../Override/Get Extinction Ratio.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'S!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!)%!Q`````R&gt;4&gt;(*J&lt;G=[28BU;7ZD&gt;'FP&lt;C"3982J&lt;Q!@1!I!'%ZV&lt;76S;7-[28BU;7ZD&gt;'FP&lt;C"3982J&lt;Q!!8%"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!93W6Z=WFH;(1A/$9R-$"%8U&gt;135)A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!=!#1-!!(A!!!U)!!!*!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!I!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
				<Item Name="Get Eye Height.vi" Type="VI" URL="../Override/Get Eye Height.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'G!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'E!Q`````R&amp;4&gt;(*J&lt;G=[28FF)%BF;7&gt;I&gt;!!:1!I!%EZV&lt;76S;7-[28FF)%BF;7&gt;I&gt;!!!8%"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!93W6Z=WFH;(1A/$9R-$"%8U&gt;135)A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!=!#1-!!(A!!!U)!!!*!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!I!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
				<Item Name="Get Eye Width.vi" Type="VI" URL="../Override/Get Eye Width.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'E!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'E!Q`````R"4&gt;(*J&lt;G=[28FF)&amp;&gt;J:(2I!!!81!I!%5ZV&lt;76S;7-[28FF)&amp;&gt;J:(2I!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!7E"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!83W6Z=WFH;(1A/$9R-$"%8U&gt;135)A;7Y!6!$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!(!!E$!!"Y!!!.#!!!#1!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!+!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
				<Item Name="Get Eye Amplitude.vi" Type="VI" URL="../Override/Get Eye Amplitude.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'M!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(E!Q`````R24&gt;(*J&lt;G=[28FF)%&amp;N='RJ&gt;(6E:1!!'U!+!"6/&gt;7VF=GFD/E6Z:3""&lt;8"M;82V:'5!8%"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!93W6Z=WFH;(1A/$9R-$"%8U&gt;135)A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!=!#1-!!(A!!!U)!!!*!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!I!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
				<Item Name="Get Eye Mask Margin.vi" Type="VI" URL="../Override/Get Eye Mask Margin.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'Q!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!)%!Q`````R:4&gt;(*J&lt;G=[28FF)%VB9WMA47&amp;S:WFO!!!&gt;1!I!&amp;UZV&lt;76S;7-[28FF)%VB9WMA47&amp;S:WFO!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!7E"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!83W6Z=WFH;(1A/$9R-$"%8U&gt;135)A;7Y!6!$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!(!!E$!!"Y!!!.#!!!#1!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!+!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
				<Item Name="Get Fall Time.vi" Type="VI" URL="../Override/Get Fall Time.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'E!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'E!Q`````R"4&gt;(*J&lt;G=[2G&amp;M&lt;#"5;7VF!!!81!I!%5ZV&lt;76S;7-[2G&amp;M&lt;#"5;7VF!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!7E"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!83W6Z=WFH;(1A/$9R-$"%8U&gt;135)A;7Y!6!$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!(!!E$!!"Y!!!.#!!!#1!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!+!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
				<Item Name="Get Rise Time.vi" Type="VI" URL="../Override/Get Rise Time.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'E!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'E!Q`````R"4&gt;(*J&lt;G=[5GFT:3"5;7VF!!!81!I!%5ZV&lt;76S;7-[5GFT:3"5;7VF!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!7E"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!83W6Z=WFH;(1A/$9R-$"%8U&gt;135)A;7Y!6!$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!(!!E$!!"Y!!!.#!!!#1!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!+!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
				<Item Name="Get Jitter Peak-Peak.vi" Type="VI" URL="../Override/Get Jitter Peak-Peak.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'S!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!)%!Q`````R&gt;4&gt;(*J&lt;G=[3GFU&gt;'6S)&amp;"F97MN5'6B;Q!@1!I!'%ZV&lt;76S;7-[3GFU&gt;'6S)&amp;"F97MN5'6B;Q!!8%"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!93W6Z=WFH;(1A/$9R-$"%8U&gt;135)A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!=!#1-!!(A!!!U)!!!*!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!I!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
				<Item Name="Get Jitter RMS.vi" Type="VI" URL="../Override/Get Jitter RMS.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'G!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'E!Q`````R&amp;4&gt;(*J&lt;G=[3GFU&gt;'6S)&amp;*.5Q!:1!I!%EZV&lt;76S;7-[3GFU&gt;'6S)&amp;*.5Q!!8%"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!93W6Z=WFH;(1A/$9R-$"%8U&gt;135)A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!=!#1-!!(A!!!U)!!!*!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!I!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
				<Item Name="Set Waveforms.vi" Type="VI" URL="../Override/Set Waveforms.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;T!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
				</Item>
				<Item Name="Get Measurement.vi" Type="VI" URL="../Override/Get Measurement.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+6!!!!&amp;A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!".!#A!.18:F=G&amp;H:3"1&lt;X&gt;F=A!01!I!#%.S&lt;X.T;7ZH!!!*1!I!!E63!!!21!I!#UVB=WMA47&amp;S:WFO!".!#A!.28FF)%&amp;N='RJ&gt;(6E:1!21!I!#E6Z:3"):7FH;(1!!!^!#A!*28FF)&amp;&gt;J:(2I!!^!#A!*5GFT:3"5;7VF!!^!#A!*2G&amp;M&lt;#"5;7VF!"&amp;!#A!+3GFU&gt;'6S)&amp;!N5!!!%5!+!!J+;82U:8)A5EV4!!!*1!I!!U^.11"K!0%!!!!!!!!!!QR49W^Q:3ZM&gt;GRJ9H!.5W.P='5O&lt;(:D&lt;'&amp;T=R"%982B,5.M&gt;8.U:8)O9X2M!$:!5!!-!!5!"A!(!!A!#1!+!!M!$!!.!!Y!$Q!1%E&amp;D9W6T=W^S/E2B&gt;'%[2'&amp;U91!!8%"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!93W6Z=WFH;(1A/$9R-$"%8U&gt;135)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!&amp;1!]!!-!!-!"!!2!")!"!!%!!1!"!!4!!1!"!!5!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!&amp;1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
				</Item>
				<Item Name="Get OMA.vi" Type="VI" URL="../Override/Get OMA.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'9!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;%!Q`````QJ4&gt;(*J&lt;G=[4UV"!!!21!I!#UZV&lt;76S;7-[4UV"!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!7E"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!83W6Z=WFH;(1A/$9R-$"%8U&gt;135)A;7Y!6!$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!(!!E$!!"Y!!!.#!!!#1!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!+!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
				</Item>
			</Item>
			<Item Name="PAM4 Measurement" Type="Folder">
				<Item Name="Get PAM4 Eye Height.vi" Type="VI" URL="../Override/Get PAM4 Eye Height.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)2!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!#A!/28FF)$!P-3"):7FH;(1!!"6!#A!/28FF)$%P-C"):7FH;(1!!"6!#A!/28FF)$)P-S"):7FH;(1!!&amp;]!]1!!!!!!!!!$$&amp;.D&lt;X"F,GRW&lt;'FC=!V49W^Q:3ZM&gt;G.M98.T'U6Z:3"):7FH;(1A2'&amp;U93V$&lt;(6T&gt;'6S,G.U&lt;!!A1&amp;!!!Q!&amp;!!9!"Q^115UU)%6Z:3"):7FH;(1!8%"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!93W6Z=WFH;(1A/$9R-$"%8U&gt;135)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!&amp;1!]!!-!!-!"!!)!!E!"!!%!!1!"!!+!!1!"!!,!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!$!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
				</Item>
				<Item Name="Get PAM4 Eye Width.vi" Type="VI" URL="../Override/Get PAM4 Eye Width.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)+!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!".!#A!.28FF)$!P-3"8;72U;!!41!I!$56Z:3!R,T)A6WFE&gt;'A!%U!+!!V&amp;?75A-C]T)&amp;&gt;J:(2I!&amp;Y!]1!!!!!!!!!$$&amp;.D&lt;X"F,GRW&lt;'FC=!V49W^Q:3ZM&gt;G.M98.T'E6Z:3"8;72U;#"%982B,5.M&gt;8.U:8)O9X2M!#"!5!!$!!5!"A!($F""441A28FF)&amp;&gt;J:(2I!!"=1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"B,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!7E"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!83W6Z=WFH;(1A/$9R-$"%8U&gt;135)A;7Y!6!$Q!!Q!!Q!%!!A!#1!%!!1!"!!%!!I!"!!%!!M$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!-!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
				</Item>
				<Item Name="Get PAM4 Level.vi" Type="VI" URL="../Override/Get PAM4 Level.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(`!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!(4'6W:7QA-!!.1!I!"URF&gt;G6M)$%!$5!+!!&gt;-:8:F&lt;#!S!!V!#A!(4'6W:7QA-Q"9!0%!!!!!!!!!!QR49W^Q:3ZM&gt;GRJ9H!.5W.P='5O&lt;(:D&lt;'&amp;T=R:-:8:F&lt;#"%982B,5.M&gt;8.U:8)O9X2M!"Z!5!!%!!5!"A!(!!A+5%&amp;..#"-:8:F&lt;!!!8%"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!93W6Z=WFH;(1A/$9R-$"%8U&gt;135)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!&amp;1!]!!-!!-!"!!*!!I!"!!%!!1!"!!,!!1!"!!-!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!$1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
				</Item>
				<Item Name="Get PAM4 TDECQ.vi" Type="VI" URL="../Override/Get PAM4 TDECQ.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'=!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;E!Q`````QR4&gt;(*J&lt;G=[6%2&amp;1V%!!".!#A!.4H6N:8*J9TJ52%6$51"=1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"B,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!"Q!*!Q!!?!!!$1A!!!E!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
				</Item>
				<Item Name="Get PAM4 Outer ER.vi" Type="VI" URL="../Override/Get PAM4 Outer ER.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'C!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'%!Q`````Q^4&gt;(*J&lt;G=[4X6U:8)A26)!&amp;U!+!""/&gt;7VF=GFD/E^V&gt;'6S)%63!!"=1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"B,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!"Q!*!Q!!?!!!$1A!!!E!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
				</Item>
				<Item Name="Get PAM4 Outer OMA.vi" Type="VI" URL="../Override/Get PAM4 Outer OMA.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'E!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'E!Q`````R"4&gt;(*J&lt;G=[4X6U:8)A4UV"!!!81!I!%5ZV&lt;76S;7-[4X6U:8)A4UV"!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!7E"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!83W6Z=WFH;(1A/$9R-$"%8U&gt;135)A;7Y!6!$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!(!!E$!!"Y!!!.#!!!#1!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!+!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
				</Item>
				<Item Name="Get PAM4 Linearity [RLM].vi" Type="VI" URL="../Override/Get PAM4 Linearity [RLM].vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'Q!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!)%!Q`````R:4&gt;(*J&lt;G=[4'FO:7&amp;S;82Z)&amp;N34%V&gt;!!!&gt;1!I!&amp;UZV&lt;76S;7-[4'FO:7&amp;S;82Z)&amp;N34%V&gt;!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!7E"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!83W6Z=WFH;(1A/$9R-$"%8U&gt;135)A;7Y!6!$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!(!!E$!!"Y!!!.#!!!#1!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!+!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
				</Item>
				<Item Name="Set Patterns.vi" Type="VI" URL="../Override/Set Patterns.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;T!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
				</Item>
				<Item Name="Get PAM4 Measurement Data.vi" Type="VI" URL="../Override/Get PAM4 Measurement Data.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!3D!!!!)!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!".!#A!.18:F=G&amp;H:3"1&lt;X&gt;F=A!01!I!#%^V&gt;'6S)%63!!!01!I!#5^V&gt;'6S)%^.11!,1!I!"62%25.2!"6!#A!04'FO:7&amp;S;82Z)&amp;N34%V&gt;!!V!#A!(4'6W:7QA-!!.1!I!"URF&gt;G6M)$%!$5!+!!&gt;-:8:F&lt;#!S!!V!#A!(4'6W:7QA-Q"3!0%!!!!!!!!!!QR49W^Q:3ZM&gt;GRJ9H!.5W.P='5O&lt;(:D&lt;'&amp;T=R:-:8:F&lt;#"%982B,5.M&gt;8.U:8)O9X2M!"B!5!!%!!I!#Q!-!!U&amp;4'6W:7Q!'5!+!"*&amp;?75A3'6J:WBU)&amp;2P=#!I*3E!!"F!#A!328FF)%BF;7&gt;I&gt;#".;71A+#5J!!!:1!I!%E6Z:3"):7FH;(1A1G^U)#AF+1!!&lt;!$R!!!!!!!!!!--5W.P='5O&lt;(:M;7*Q$6.D&lt;X"F,GRW9WRB=X-E5%&amp;..#"&amp;?75A3'6J:WBU)#AF+3"%982B,5.M&gt;8.U:8)O9X2M!#2!5!!$!!]!%!!2%V""441A28FF)%BF;7&gt;I&gt;#!I*3E!&amp;5!+!!Z&amp;?75A-#]R)%BF;7&gt;I&gt;!!!&amp;5!+!!Z&amp;?75A-3]S)%BF;7&gt;I&gt;!!!&amp;5!+!!Z&amp;?75A-C]T)%BF;7&gt;I&gt;!!!8Q$R!!!!!!!!!!--5W.P='5O&lt;(:M;7*Q$6.D&lt;X"F,GRW9WRB=X-&lt;28FF)%BF;7&gt;I&gt;#"%982B,5.M&gt;8.U:8)O9X2M!#"!5!!$!"-!&amp;!!6$V""441A28FF)%BF;7&gt;I&gt;!!41!I!$56Z:3!Q,T%A6WFE&gt;'A!%U!+!!V&amp;?75A-3]S)&amp;&gt;J:(2I!".!#A!.28FF)$)P-S"8;72U;!"?!0%!!!!!!!!!!QR49W^Q:3ZM&gt;GRJ9H!.5W.P='5O&lt;(:D&lt;'&amp;T=RJ&amp;?75A6WFE&gt;'AA2'&amp;U93V$&lt;(6T&gt;'6S,G.U&lt;!!A1&amp;!!!Q!8!"A!'1Z115UU)%6Z:3"8;72U;!!!&lt;1$R!!!!!!!!!!--5W.P='5O&lt;(:M;7*Q$6.D&lt;X"F,GRW9WRB=X-65%&amp;..#"%982B,5.M&gt;8.U:8)O9X2M!$2!5!!*!!5!"A!(!!A!#1!/!")!&amp;A!;&amp;U&amp;D9W6T=W^S/F""441A2'&amp;U94J115UU!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!'Q!=!!1!"!!%!!1!(1!%!!1!(A-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!"]!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
				</Item>
				<Item Name="Get PAM4 Rx Eye Data.vi" Type="VI" URL="../Override/Get PAM4 Rx Eye Data.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;T!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
				</Item>
			</Item>
			<Item Name="Public" Type="Folder">
				<Item Name="Select Measurement Mode.vi" Type="VI" URL="../Public/Select Measurement Mode.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(N!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"[!0%!!!!!!!!!!QR49W^Q:3ZM&gt;GRJ9H!.5W.P='5O&lt;(:D&lt;'&amp;T=RF.:7&amp;T&gt;8*F&lt;76O&gt;#".&lt;W2F,56O&gt;7UO9X2M!$V!&amp;A!$#6&gt;B&gt;G6G&lt;X*N=QJ&amp;?75A6(6O;7ZH#E.P&lt;H2J&lt;H6P&gt;8-!%%VF98.V=G6N:7ZU)%VP:'5!!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#1!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
				</Item>
				<Item Name="Run Measurement.vi" Type="VI" URL="../Override/Run Measurement.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;T!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Initialize Config.vi" Type="VI" URL="../Override/Initialize Config.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;T!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
		</Item>
		<Item Name="Open.vi" Type="VI" URL="../Override/Open.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;T!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Reset.vi" Type="VI" URL="../Override/Reset.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;T!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Configure.vi" Type="VI" URL="../Override/Configure.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;T!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Save Screen.vi" Type="VI" URL="../Override/Save Screen.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'8!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;:!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!%UNF?8.J:WBU)$AW-4!Q2#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!;1$$`````%5:J&lt;'5A4G&amp;N:3AK,GJQ:7=J!"2!-P````],2G^M:'6S)&amp;"B&gt;'A!6E"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!33W6Z=WFH;(1A/$9R-$"%)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!A!#1-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!1I!!!!)!!!!E!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Close.vi" Type="VI" URL="../Override/Close.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;T!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Convert Data.vi" Type="VI" URL="../Private/Convert Data.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%!!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-0````]-1W^O&gt;G6S&gt;#"%982B!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!)1R%;8.Q&lt;'&amp;Z)&amp;6O;81!!!Z!-0````]%67ZJ&gt;!!!$5!+!!&gt;/&gt;7VF=GFD!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!Q!!?!!!$1A!!!!!!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!#!!!!AA!!!!+!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Convert Reply.vi" Type="VI" URL="../Private/Convert Reply.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%P!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%%!B#EZP)&amp;.J:WZB&lt;$]!!"*!-0````]*5G6Q&lt;(EA&lt;X6U!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!-0````])5G6Q&lt;(EA;7Y!!$Z!=!!?!!!F&amp;EFO=X2S&gt;7VF&lt;H1A1W^N&lt;3ZM&gt;GRJ9H!-6EF413ZM&gt;G.M98.T!!^736.",GRW9WRB=X-A;7Y!6!$Q!!Q!!Q!%!!5!"A!'!!9!"A!'!!=!"A!)!!E$!!"Y!!!.#!!!#1!!!!U+!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!"#A!!!!A!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
	</Item>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Acquisition Setup" Type="Folder">
			<Item Name="Acquisition Setup - Set Number of Waveforms.vi" Type="VI" URL="../Public/Acquisition Setup - Set Number of Waveforms.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!':!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!:1!I!%UZV&lt;7*F=C"P:C"898:F:G^S&lt;8-!7E"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!83W6Z=WFH;(1A/$9R-$"%8U&gt;135)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!!A!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710290</Property>
			</Item>
			<Item Name="Rapid Eye Enable Align Channels During Auto Scale.vi" Type="VI" URL="../Public/Rapid Eye Enable Align Channels During Auto Scale.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'-!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!-1#%'4UYP4U:'!!";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!1!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!A!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Rapid Eye Enable.vi" Type="VI" URL="../Public/Rapid Eye Enable.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'-!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!-1#%'4UYP4U:'!!";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!1!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!A!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
		</Item>
		<Item Name="Channel" Type="Folder">
			<Item Name="Add Channel to Command.vi" Type="VI" URL="../Public/Add Channel to Command.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'U!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!-0````],1W^N&lt;7&amp;O:#"P&gt;81!8%"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!93W6Z=WFH;(1A/$9R-$"%8U&gt;135)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!R!)1:*&lt;H:F=H1!!"2!-0````]+1W^N&lt;7&amp;O:#"J&lt;A!!7E"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!83W6Z=WFH;(1A/$9R-$"%8U&gt;135)A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!)!!!"#A!!!!A!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Channel Display.vi" Type="VI" URL="../Public/Channel Display.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'-!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!-1#%'4UYP4U:'!!";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!1!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!A!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Channel Filter State.vi" Type="VI" URL="../Public/Channel Filter State.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'-!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!-1#%'4UYP4U:'!!";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!1!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!A!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Channel Get Status.vi" Type="VI" URL="../Public/Channel Get Status.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1J$&lt;WZO:7.U:71`!!"=1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"B,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!7E"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!83W6Z=WFH;(1A/$9R-$"%8U&gt;135)A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Channel Select Bandwidth Frequency.vi" Type="VI" URL="../Public/Channel Select Bandwidth Frequency.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'0!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!01!I!#6*B&gt;'5I2UB[+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!+!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Channel Select Eye Power.vi" Type="VI" URL="../Public/Channel Select Eye Power.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
			</Item>
			<Item Name="Channel Select Eye Source.vi" Type="VI" URL="../Public/Channel Select Eye Source.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Channel Select Filter Rate.vi" Type="VI" URL="../Public/Channel Select Filter Rate.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'2!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!21!I!#F*B&gt;'5I2W)P=SE!!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Channel Set Wavelength.vi" Type="VI" URL="../Public/Channel Set Wavelength.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'6!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!61!I!$F&gt;B&gt;G6M:7ZH&gt;'AI&lt;GUJ!!";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="Config TDECQ Eq.vi" Type="VI" URL="../Public/Config TDECQ Eq.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Configure Channel.vi" Type="VI" URL="../Override/Configure Channel.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			</Item>
			<Item Name="Configure Display Screen.vi" Type="VI" URL="../Override/Configure Display Screen.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Configure Mask Test.vi" Type="VI" URL="../Override/Configure Mask Test.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
		</Item>
		<Item Name="Configure Base Measurements" Type="Folder">
			<Item Name="Threshold Definition" Type="Folder">
				<Item Name="Measure Threshold Method.vi" Type="VI" URL="../Public/Measure Threshold Method.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)F!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$E!Q`````Q63:8"M?1!%!!!!8%"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!93W6Z=WFH;(1A/$9R-$"%8U&gt;135)A&lt;X6U!!!+1#%%5G6B:!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1#.!0%!!!!!!!!!!RJ,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T*5VF98.V=G5A6'BS:7BP&lt;'1A476U;'^E)&amp;2Z='5N27ZV&lt;3ZD&gt;'Q!*U!7!!-(5$%Q.4!Z-!&gt;1-D!V-$AQ"&amp;6%259!!!25?8"F!!";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"1!'!!5!"1!(!!5!#!!*!!5!#A-!!(A!!!U)!!!*!!!!!!!!!!U,!!!!!!!!!!!!!!A!!!!!!!!!#A!!!!A!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Display" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Graticule" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="Display Setup - Set Waveform Persistence.vi" Type="VI" URL="../Public/Display Setup - Set Waveform Persistence.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!);!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1#;!0%!!!!!!!!!!RJ,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T&amp;&amp;"F=H.J=X2F&lt;G.F,56O&gt;7UO9X2M!%6!&amp;A!##U.P&lt;'^S)%&gt;S972F#E&gt;S98EA5W.B&lt;'5!!#&amp;1:8*T;8.U:7ZD:3!I5W&amp;N='RF)%&amp;D9X6N&gt;7RB&gt;'FP&lt;CE!7E"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!83W6Z=WFH;(1A/$9R-$"%8U&gt;135)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
				</Item>
				<Item Name="Display Setup - Set Grid.vi" Type="VI" URL="../Public/Display Setup - Set Grid.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'8!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!01!=!#5FO&gt;'6O=WFU?1!)1#%#4WY!!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!!A!!!!)!!!.!!!!$!!!!!!!!!!!!!!"!!I!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
				</Item>
				<Item Name="Display Setup - Set Maximum Samples.vi" Type="VI" URL="../Public/Display Setup - Set Maximum Samples.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'6!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!61!I!$UVB?'FN&gt;7UA5W&amp;N='RF=Q";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!#!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
			</Item>
			<Item Name="Appearance" Type="Folder">
				<Item Name="Display Setup - Set Invert Signal Display Windows.vi" Type="VI" URL="../Public/Display Setup - Set Invert Signal Display Windows.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'-!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!-1#%'37ZW:8*U!!";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!#!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
			</Item>
			<Item Name="Results" Type="Folder">
				<Item Name="Display Setup - Set Results Window.vi" Type="VI" URL="../Public/Display Setup - Set Results Window.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)X!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1#X!0%!!!!!!!!!!RJ,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T&amp;6&gt;J&lt;G2P&gt;S"4&gt;'&amp;U:3V&amp;&lt;H6N,G.U&lt;!"B1"9!"!J'&gt;7RM)&amp;&gt;J:(2I#EBB&lt;'9A6WFE&gt;'A947^S:3"*&lt;H2P)&amp;"S;7VB=HEA6WFO:'^X'%VP&gt;G5A&gt;']A5W6D&lt;WZE98*Z)&amp;&gt;J&lt;G2P&gt;Q!-6WFO:'^X)&amp;.U982F!!";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Mask Test" Type="Folder">
			<Item Name="Mask Test Display Status.vi" Type="VI" URL="../Public/Mask Test Display Status.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'J!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"B!)2..98.L)%2J=X"M98EA5X2B&gt;(6T!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!21!-!#UVB=WMA+$%N-49J!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Mask Test Display.vi" Type="VI" URL="../Public/Mask Test Display.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!')!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!)1#%$4UY`!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!"!!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Mask Test Get Load File.vi" Type="VI" URL="../Public/Mask Test Get Load File.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'9!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"B!-0````]/47&amp;T;S"';7RF)%ZB&lt;75!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Mask Test Load File.vi" Type="VI" URL="../Public/Mask Test Load File.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'&lt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!&lt;1"9!!1B&amp;&gt;'BF=GZF&gt;!!!#5VB=WMA6(FQ:1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			</Item>
			<Item Name="Mask Test Margin Hit Count Value.vi" Type="VI" URL="../Public/Mask Test Margin Hit Count Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!',!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!,1!I!"6:B&lt;(6F!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!"!!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Mask Test Margin Hit Ratio Value.vi" Type="VI" URL="../Public/Mask Test Margin Hit Ratio Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!',!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!,1!I!"6:B&lt;(6F!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!"!!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Mask Test Margin Hit Type.vi" Type="VI" URL="../Public/Mask Test Margin Hit Type.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'=!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!=1#%86$V);81A5G&amp;U;7]+2DV);81A1W^V&lt;H1!7E"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!83W6Z=WFH;(1A/$9R-$"%8U&gt;135)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"Q!%!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!)!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Mask Test Margin State.vi" Type="VI" URL="../Public/Mask Test Margin State.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!')!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!)1#%$4UY`!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!"!!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Mask Test Margin Type.vi" Type="VI" URL="../Public/Mask Test Margin Type.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!')!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!)1#%$4UY`!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!"!!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Mask Test Select Source.vi" Type="VI" URL="../Public/Mask Test Select Source.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
		</Item>
		<Item Name="Pattern Lock" Type="Folder">
			<Item Name="Get Pattern Lock.vi" Type="VI" URL="../Public/Get Pattern Lock.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'3!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!)1R1982U:8*O)%RP9WM!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!#!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Set Pattern Lock.vi" Type="VI" URL="../Public/Set Pattern Lock.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'3!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!31#%-5'&amp;U&gt;'6S&lt;C"-&lt;W.L!!";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!#!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			</Item>
		</Item>
		<Item Name="Slot Trigger" Type="Folder">
			<Item Name="Slot Trigger Get Source.vi" Type="VI" URL="../Public/Slot Trigger Get Source.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!';!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'E!Q`````R&amp;4&gt;(*J&lt;G=[1WRP9WMA5G&amp;U:1!%!!!!8%"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!93W6Z=WFH;(1A/$9R-$"%8U&gt;135)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!'%!]!!-!!-!"!!&amp;!!9!"1!&amp;!!5!"1!(!!5!"1!)!Q!!?!!!$1A!!!E!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Slot Trigget Get Clock Rate.vi" Type="VI" URL="../Public/Slot Trigget Get Clock Rate.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'Z!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'E!Q`````R&amp;4&gt;(*J&lt;G=[1WRP9WMA5G&amp;U:1!@1!I!'%ZV&lt;76S;7-[1WRP9WMA5G&amp;U:3!I2UB[+1!!8%"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!93W6Z=WFH;(1A/$9R-$"%8U&gt;135)A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!=!#1-!!(A!!!U)!!!*!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!+!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
		</Item>
		<Item Name="System" Type="Folder">
			<Item Name="System Show Display Status.vi" Type="VI" URL="../Public/System Show Display Status.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'9!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'%!Q`````QZ%;8.Q&lt;'&amp;Z)&amp;.U982V=Q!!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"1!'!!5!"1!&amp;!!5!"Q!&amp;!!5!#!-!!(A!!!U)!!!*!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
		</Item>
		<Item Name="Timebase" Type="Folder">
			<Item Name="Timebase Symbol Rate.vi" Type="VI" URL="../Public/Timebase Symbol Rate.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'.!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!.1!I!"V*B&gt;'5I2SE!7E"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!83W6Z=WFH;(1A/$9R-$"%8U&gt;135)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"Q!%!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!+!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
		</Item>
		<Item Name="Waveform" Type="Folder">
			<Item Name="Pattern" Type="Folder">
				<Item Name="Waveform Pattern Get Complete Status.vi" Type="VI" URL="../Public/Waveform Pattern Get Complete Status.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1F$&lt;WVQ&lt;'6U:4]!8%"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!93W6Z=WFH;(1A/$9R-$"%8U&gt;135)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Auto Scale.vi" Type="VI" URL="../Override/Auto Scale.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Clear Display.vi" Type="VI" URL="../Override/Clear Display.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Filter Measurement Data.vi" Type="VI" URL="../Public/Filter Measurement Data.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'C!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!-0````])2'&amp;U93"P&gt;81!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!11$$`````"U2B&gt;'%A;7Y!7E"Q!"Y!!$E;3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:M;7)=3W6Z=WFH;(1A/$9R-$"%8U&gt;135)O&lt;(:D&lt;'&amp;T=Q!83W6Z=WFH;(1A/$9R-$"%8U&gt;135)A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!)!!E#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!##!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="Get Math TDECQ EQ State.vi" Type="VI" URL="../Public/Get Math TDECQ EQ State.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
		<Item Name="Mask File Exist.vi" Type="VI" URL="../Public/Mask File Exist.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'3!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!31$$`````#5VB=WMA6(FQ:1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!))!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821074</Property>
		</Item>
		<Item Name="Run.vi" Type="VI" URL="../Override/Run.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Set Mode.vi" Type="VI" URL="../Public/Set Mode.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1#*!0%!!!!!!!!!!RJ,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T$5VP:'5N27ZV&lt;3ZD&gt;'Q!/U!7!!1)28FF,UVB=WM,4X.D;7RP=W.P='5(6%23,V2%6!R+;82U:8)P4G^J=W5!"%VP:'5!!&amp;J!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!&amp;UNF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Stop.vi" Type="VI" URL="../Override/Stop.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'!!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!Z'ENF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW&lt;'FC(%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#,GRW9WRB=X-!'%NF?8.J:WBU)$AW-4!Q2&amp;^(5%F#)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/2J,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;GRJ9BR,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1CZM&gt;G.M98.T!"&gt;,:8FT;7&gt;I&gt;#!Y.D%Q-%2@2V"*1C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
	</Item>
</LVClass>
